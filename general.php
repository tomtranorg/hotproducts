<link rel="stylesheet " type="text/css" href="../modules/promoteProducts/css/style.css" />
<script type = "text/javascript" src = "../modules/promoteProducts/js/admin.js"></script>

<center><h2>General</h2></center>

<?php
                                          
                /*
                 * Round number - product's price
                 */
                if( !function_exists('ceiling') )
                    {
                        function ceiling($number, $significance = 1)
                        {
                            return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
                        }
                    }
                
               /*
                * Update product's status
                */                         
                $url_recent = $_SERVER['REQUEST_URI'];
               
                if(isset($_POST["cmd"]))
                {
                     foreach($_POST['mang2'] as $key=>$value)
                    {
                             $sql2="UPDATE ps_product_lang SET status=0 WHERE id_product=$key";
                             mysql_query($sql2);
                             //Tools::redirectAdmin($url_recent);
                			
                    }
                            
                }
                
                if(count($_POST['mang'])>0)
                {
                    foreach($_POST['mang'] as $key=>$value)
                    {
                             $sql="UPDATE ps_product_lang SET status=$value WHERE id_product=$key";
                           	 mysql_query($sql);
            				
                    }
                }
                
                        /*
                         * Update radio button's value
                         */        
                        if($_POST["feature"])
                        {
                            Configuration::updateValue('feature',$_POST["feature"]);
                                                       
                        }
                        
                        /*
                         * Update number of product's display in Front Office
                         */
                        if (Tools::isSubmit("cmd"))
                        {
                            if($_POST["number_cheap"])
                            {
                                Configuration::updateValue('number_cheap',$_POST["number_cheap"]);
                                                         
                            }
                            
                        
                            
                             if($_POST["number_new"])
                            {
                                Configuration::updateValue('number_new',$_POST["number_new"]);
                                                         
                            }
                             
                            
                            
                             if($_POST["number_popular"])
                            {
                                Configuration::updateValue('number_popular',$_POST["number_popular"]);
                                                         
                            }
                             
                            
                            
                             if($_POST["number_bestsell"])
                            {
                                Configuration::updateValue('number_bestsell',$_POST["number_bestsell"]);
                                                         
                            }
                            
                            
                            
                             if($_POST["number_random"])
                            {
                                Configuration::updateValue('number_random',$_POST["number_random"]);
                                                         
                            }
                             
                        }
                   
                
                /*
                 * Update image's url.
                 */
                $sql_img = "SELECT id_image FROM "._DB_PREFIX_."image WHERE position=1";
    			$img_query = mysql_query($sql_img);
    			$images = array();
                
        		while($id_image = mysql_fetch_array($img_query))
                    {
        				$image = new Image($id_image['id_image']);
                		$image_url = _THEME_PROD_DIR_.$image->getExistingImgPath();
                        mysql_query("Update "._DB_PREFIX_."image set url='$image_url' where id_image='$id_image[id_image]'");
        				
        				
        			}
                
                
                /*
                 * SQL Query
                 */
                 
                $trang=$_GET['trg'];
                $a=($trang-1)*5;
                
                $sql_product = 'select ps_product.date_add as date, 
                        ps_image.url as url, ps_product.price as price, 
                        ps_product.id_category_default, 
                        ps_product.id_product, 
                        ps_product.quantity, 
                        ps_product_lang.name as product_name, 
                        ps_product_lang.id_product as product_id, 
                        ps_product_lang.id_lang, 
                        ps_product_lang.status as stt, 
                        ps_category_lang.id_category as category_id, 
                        ps_category_lang.id_lang, 
                        ps_category_lang.name as category_name 
                        
                        FROM ps_image, ps_category_lang, ps_product, ps_product_lang
                        
                        WHERE  ps_product.id_category_default = ps_category_lang.id_category
                        AND    ps_product.id_product = ps_product_lang.id_product
                        AND    ps_image.id_product = ps_product.id_product
                        AND    ps_product_lang.id_lang = 1
                        AND    ps_image.position = 1
                        AND    ps_category_lang.id_lang = 1';
               
                
                
                if ($_POST['search'])
                    {
                        
                        if ($_POST['id_product'])
                            {
                                $sql_product .= " AND ps_product_lang.id_product like '%$_POST[id_product]%'";
                            }
                          
                        if ($_POST['name_product'])
                            {
                                $sql_product .= " AND ps_product_lang.name like '%$_POST[name_product]%'";
                            }
                            
                        if ($_POST['name_category'])
                            {
                                $sql_product .= " AND ps_category_lang.name like '%$_POST[name_category]%'";
                            }
                    }
                    
                 
                if(isset($_POST['category']) && $_POST['category'] != '')
                    {
 
                           
                            $category_filter = Tools::getValue('category');
                            
                            $sql_product .=" AND ps_category_lang.name = '$category_filter'";
                        
                    }
                    
               
                
                if($_GET['c'] && $_GET['c'] != '')
                    {
                        $sql_product .=" AND ps_category_lang.name = '$_GET[c]'";
                    }
                
                
                    if($_POST['feature'] == 'cheapest' OR Configuration::get('feature') == 'cheapest')
                        {
                            $sql_product .= " ORDER BY price ASC limit $a,5";
                            
                        }
                    elseif($_POST['feature'] == 'popular' OR Configuration::get('feature') == 'popular')
                        {
                            $sql_product .= " ORDER BY ps_product.quantity DESC limit $a,5";
                        }
                    elseif($_POST['feature'] == 'normal' OR Configuration::get('feature') == 'normal')
                        {
                            $sql_product .=" ORDER BY ps_product.id_product ASC limit $a,5";
                        }
                    elseif($_POST['feature'] == 'random' OR Configuration::get('feature') == 'random')
                        {
                            $sql_product .= " ORDER BY RAND() limit $a,5";
                        }
                    elseif($_POST['feature'] == 'new' OR Configuration::get('feature') == 'new')
                        {
                            $sql_product .= " ORDER BY date DESC limit $a,5";
                        }
                    elseif($_POST['feature'] == 'bestsell' OR Configuration::get('feature') == 'bestsell')
                        {
                            $sql_product = " select ps_product_sale.quantity as quantity_sale, ps_image.url as url, ps_product.price as price, ps_product.id_category_default, ps_product.id_product, ps_product.quantity, ps_product_lang.name as product_name, ps_product_lang.id_product as product_id, ps_product_lang.id_lang, ps_product_lang.status as stt, ps_category_lang.id_category as category_id, ps_category_lang.id_lang, ps_category_lang.name as category_name FROM ps_product_sale, ps_image, ps_category_lang, ps_product, ps_product_lang
                        WHERE  ps_product.id_category_default = ps_category_lang.id_category
                        AND    ps_product.id_product = ps_product_lang.id_product
                        AND    ps_image.id_product = ps_product.id_product
                        AND    ps_product.id_product = ps_product_sale.id_product
                        AND    ps_product_lang.id_lang = 1
                        AND    ps_image.position = 1
                        AND    ps_category_lang.id_lang = 1
                        ORDER BY ps_product_sale.quantity DESC limit $a,5";
                        }
                    
                    else
                        {
                            $sql_product .=" ORDER BY ps_product.id_product ASC limit $a,5";
                        }
              
                /*
                 * Execute Sql query
                 */
                $query_product = mysql_query($sql_product);
                
                /*
                 * Product's display form.
                 */
                                             
                echo "<form name='select' method='POST' action='' >";
                
                 
                 if($_POST['feature'] == 'cheapest' OR Configuration::get('feature') == 'cheapest')
                                {
                                ?>
                                Number of product display in F.O: 
                                <input name="number_cheap" value="<?php echo Configuration::get('number_cheap');?>"/>
                            <?php } 
                 elseif($_POST['feature'] == 'new' OR Configuration::get('feature') == 'new')
                                {
                                ?>
                                Number of product display in F.O:
                                <input name="number_new" value="<?php echo Configuration::get('number_new');?>"/>
                            <?php } 
                 elseif($_POST['feature'] == 'popular' OR Configuration::get('feature') == 'popular')
                                {
                                ?>
                                Number of product display in F.O:
                                <input name="number_popular" value="<?php echo Configuration::get('number_popular');?>"/>
                            <?php } 
                 elseif($_POST['feature'] == 'bestsell' OR Configuration::get('feature') == 'bestsell')
                                {
                                ?>
                               Number of product display in F.O:
                                <input name="number_bestsell" value="<?php echo Configuration::get('number_bestsell');?>"/>
                            <?php } 
                  elseif($_POST['feature'] == 'random' OR Configuration::get('feature') == 'random')
                                {
                                ?>
                                 Number of product display in F.O: 
                                <input name="number_random" value="<?php echo Configuration::get('number_random');?>"/>
                            <?php } ?>
                  
                
                
                <table width='100%' class="table"><tr>
                                
                <td><input type='radio' name='feature' value='normal' onchange='locdm()' <?php if($_POST['feature']=='normal' OR Configuration::get('feature') == 'normal') echo "checked"; //else echo "checked"; ?>/>&nbsp;&nbsp;Featured Products</td>
                <td><input type='radio' name='feature' value='cheapest' onchange='locdm()' <?php if($_POST['feature']=='cheapest' OR Configuration::get('feature') == 'cheapest') echo "checked"; ?> />&nbsp;&nbsp;Cheap Products</td>
                <td><input type='radio' name='feature' value='new' onchange='locdm()' <?php if($_POST['feature']=='new' OR Configuration::get('feature') == 'new') echo "checked"; ?>/>&nbsp;&nbsp;New Products</td>
                <td><input type='radio' name='feature' value='popular' onchange='locdm()' <?php if($_POST['feature']=='popular' OR Configuration::get('feature') == 'popular') echo "checked"; ?>/>&nbsp;&nbsp;Most Popular</td>
                <td><input type='radio' name='feature' value='bestsell' onchange='locdm()' <?php if($_POST['feature']=='bestsell' OR Configuration::get('feature') == 'bestsell') echo "checked"; ?>/>&nbsp;&nbsp;Best Seller</td>
                <td><input type='radio' name='feature' value='random' onchange='locdm()' <?php if($_POST['feature']=='random' OR Configuration::get('feature') == 'random') echo "checked"; ?>/>&nbsp;&nbsp;Random</td>
                
                <?php
                echo "</tr></table>";            
                
                echo '<table  width="100%" class="table"><tr><td>';
                //Show danh muc de select
                $sql=mysql_query("select distinct name from ps_category_lang where id_lang=1 order by id_category ");
                echo "<select name='category' onchange='locdm()'>";
                echo "<option value=''>Select category</option>";
                //echo "<option value=''>All Products</option>";
              
               
                while($r=mysql_fetch_array($sql))
                {
               
                $id=$r["id_category"];
                $name=$r["name"];
              
                ?>
                <option value="<?php echo $name; ?>" <?php if($_REQUEST['category'] == $r["name"]) { if(isset($_REQUEST['cmd'])) {echo " selected";} else {echo " selected";}} ?> ><?php echo $name  ?></option>;
                <?php
                }
                ?>
                </select>
                </td>
                               
                <td>ID <input name="id_product" <?php if (isset($_POST['search'])){echo "value='$_POST[id_product]'";}?> /></td>
                <td> Name <input name="name_product" <?php if (isset($_POST['search'])){echo "value='$_POST[name_product]'";}?> /></td>
                <td> Category <input name="name_category" <?php if (isset($_POST['search'])){echo "value='$_POST[name_category]'";}?> /></td>
                <td align="right"><input type="submit" name="search" value="Search"/></td>
                </tr></table>
                
                <?php 
                $cate = $_REQUEST['category'];
                
                //Fetching product
                $num = mysql_num_rows($query_product);
                if($num == 0)
                    {
                        echo "<center><strong>No product !</strong></center>";
                    }
                else
                {
                
                echo '<center><table border="1" width="100%" class="table">';
                
                ?>
                <tr><td><b><center>ID</center></b></td>
                <td><b><center>Product Name</center></b></td>
                <td><b><center>Price</center></b></td>
                <td><b><center>Quantity</center></b></td>
                <?php if($_POST['feature'] == 'bestsell' OR Configuration::get('feature') == 'bestsell')
                    {
                        echo '<td><b><center>Quantity Sold</center></b></td>';
                    }
                    elseif($_POST['feature'] == 'new' OR Configuration::get('feature') == 'new')
                    {
                        echo '<td><b><center>Date Time</center></b></td>';
                    }
                ?>
                
                <td><b><center>Categories</center></b></td>
                <?php if($_POST['feature'] == 'normal' OR Configuration::get('feature') == 'normal')
                    {
                        echo '<td><b><center>Status</center></b></td>';
                    }
                ?>
                </tr>
                <?php
                                              
                while($product = mysql_fetch_array($query_product))
                    {
                        $product_name = $product['product_name'];
                        $category_name = $product['category_name'];
                        $product_id = $product['product_id'];
                        $stt = $product['stt'];
                        $category_id = $product['category_id'];
                        $price_product2 = $product['price'];
                        $price_product = ceiling($price_product2,0.01);
                        $quantity = $product['quantity'];
                        $url = $product['url'];
                        $quantity_sale = $product['quantity_sale'];
                        $date = $product['date'];
                                                
                        echo '
                        <tr>
                        <td width="4%"><center>'.$product_id.'</center></td>
                        <td width="45%"><img src="'.$url.'-small.jpg" width=30 height=30/>&nbsp;&nbsp;<a href="index.php?tab=AdminCatalog&id_product='.$product_id.'&updateproduct&token=817efb645eb3d0c159c3c9472314a2cc" title="'.$product_name.'">'.$product_name.'</a></td>
                        <td width="10%"><center><font color="#004990">'.$price_product.' $</font></center></td>
                        <td width="10%"><center><font color="#3B5998">'.$quantity.'</font></center></td>';
                        ?>
                        <?php if($_POST['feature'] == 'bestsell' OR Configuration::get('feature') == 'bestsell') {echo '<td width="10%"><center><font color="#3B5998">'.$quantity_sale.'</font></center></td>';}
                        elseif($_POST['feature'] == 'new' OR Configuration::get('feature') == 'new') {echo '<td width="10%"><center><font color="#3B5998">'.$date.'</font></center></td>';}
                        ?>
                        
                        <td width="17%"><a href="<?php echo $url_recent.'&c='.$category_name ?>" title=" <?php echo $category_name ?>" target="_blank"> <?php echo $category_name ?></td>
                        
                        
                        <?php if($_POST['feature'] == 'normal' OR isset($_GET['c']) OR Configuration::get('feature') == 'normal')
                        { ?>
                        <td width="4%">
                        <center><input name='mang[<?php echo $product_id?>]' type='checkbox'  value="1"  <?php if($stt=="1"){?>  checked="checked"    <?php }else{?> <?php }?> />
                        <input type="text" style="display: none;" name='mang2[<?php echo $product_id;?>]' value="<?php echo $stt ?>" /></center>
                        </td>
                        <?php } ?>
                        
                        
                        </tr>
                        
                        
                       
                    <?php                         
                    }
                                  
                echo '</table>';
               
                        echo '<table class="table_border" width="100%"><tr><td align="right"><input type="submit" name="cmd" value="Save"/></font></td></tr></table>';
                     
                        $url = $_SERVER['REQUEST_URI'];
                $tr=intval($num/5)+1;
                $trg=0;
                echo "<b>Go to page:</b>";
                for($i=0; $i<$tr; $i++)
                {
                $trg+=1;
                echo"<font color='blue'><a href=$url&trg=$trg> $trg</a></font>&nbsp;";
                }
                echo "</center></form>";    
                }
                    
           
?>


