<link rel="stylesheet " type="text/css" href="../modules/promoteProducts/css/style.css" />
<script type = "text/javascript" src = "../modules/promoteProducts/js/admin.js"></script>

<center><h2>Option</h2></center>
<form method="POST" action="">
<table class="table_noborder" width="100%">
<tr>
<td align="center"><h4>Hook Header</h4></td>
<td align="center"><h4>Hook Home</h4></td>
</tr>

<tr>
<td width="50%" align="center">

    <table class="table" width="95%">
    <tr><td>Enable/Disable hook</td>
    <td><center>
    <input type="checkbox" name="hook_header" <?php if(Configuration::get('hook_header') == "on" or $_POST['hook_header'] == "on") { echo " checked";} ?>/> &nbsp;
        
    </center></td></tr>
    
    <tr>
    <td width="40%">List title</td>
    <td width="60%">
    <select name="list_title_header">
    <option value="normal" <?php if($_POST['list_title_header'] == 'normal' OR Configuration::get('list_title_header') == 'normal') {echo ' selected="selected"';}?> >Feature Products</option>
    <option value="cheap" <?php if($_POST['list_title_header'] == 'cheap' OR Configuration::get('list_title_header') == 'cheap') {echo ' selected="selected"';}?> >Cheap Products</option>
    <option value="new" <?php if($_POST['list_title_header'] == 'new' OR Configuration::get('list_title_header') == 'new') {echo ' selected="selected"';}?> >New Products</option>
    <option value="popular" <?php if($_POST['list_title_header'] == 'popular' OR Configuration::get('list_title_header') == 'popular') {echo " selected='selected'";}?> >Most Popular</option>
    <option value="best" <?php if($_POST['list_title_header'] == 'best' OR Configuration::get('list_title_header') == 'best') {echo " selected='selected'";}?> >Best Seller</option>
    <option value="random" <?php if($_POST['list_title_header'] == 'random' OR Configuration::get('list_title_header') == 'random') {echo " selected='selected'";}?> >Random</option>
    </select>
    </td>
    </tr>
    
   
    <tr><td>Product's price</td>
    <td align="center">    
    <input type="checkbox" name="product_price_header" <?php if($_POST['product_price_header'] == "on" OR Configuration::get('option_price_header') == "on") {echo " checked";} ?>/> &nbsp;
    </td></tr>
    
    <tr>
    <td>Add to cart button</td>
    <td align="center">
    <input type="checkbox" name="addtocart_header" <?php if($_POST['addtocart_header'] == "on" OR Configuration::get('option_cart_header') == "on") {echo " checked";} ?>/> &nbsp;
    </td>
    </tr>
    
    </table>
 </td>
 
 <td width="50%" align="center">  

    <table class="table" width="95%">
    <tr><td>Enable/Disable hook</td>
    <td align="center">
    <input type="checkbox" name="hook_home" <?php if($_POST['hook_home'] == "on" OR Configuration::get('hook_home') == "on") { echo " checked";} ?>/> &nbsp;
    </td></tr>
    
    <tr>
    <td width="40%">List title</td>
    <td width="60%">
    <select name="list_title_home">
    <option value="normal" <?php if($_POST['list_title_home'] == 'normal' OR Configuration::get('list_title_home') == "normal") {echo " selected='selected'";}?> >Feature Products</option>
    <option value="cheap" <?php if($_POST['list_title_home'] == 'cheap' OR Configuration::get('list_title_home') == "cheap") {echo " selected='selected'";}?> >Cheap Products</option>
    <option value="new" <?php if($_POST['list_title_home'] == 'new' OR Configuration::get('list_title_home') == "new") {echo " selected='selected'";}?> >New Products</option>
    <option value="popular" <?php if($_POST['list_title_home'] == 'popular' OR Configuration::get('list_title_home') == "popular") {echo " selected='selected'";}?> >Most Popular</option>
    <option value="best" <?php if($_POST['list_title_home'] == 'best' OR Configuration::get('list_title_home') == "best") {echo " selected='selected'";}?> >Best Seller</option>
    <option value="random" <?php if($_POST['list_title_home'] == 'random' OR Configuration::get('list_title_home') == "random") {echo " selected='selected'";}?> >Random</option>
    </select>
    </td>
    </tr>
    
 
    <tr><td>Product's price</td>
    <td align="center">
    <input type="checkbox" name="product_price_home" <?php if($_POST['product_price_home'] == "on" OR Configuration::get('option_price_home') == "on") {echo " checked";} ?>/> &nbsp;
    </td></tr>
    <tr>
    <td>Add to cart button</td>
    <td align="center">    
    <input type="checkbox" name="addtocart_home" <?php if($_POST['addtocart_home'] == "on" OR Configuration::get('option_cart_home') == "on") {echo " checked";} ?>/> &nbsp;
    </td>
    </tr>
    </table>
    </td>
    </tr>
    
    
    <tr>
    <td align="center"><h4>Hook Left</h4></td>
    <td align="center"><h4>Hook Right</h4></td>
    </tr>
    
    <tr>
    <td align="center">
    <table class="table" width="95%">
    <tr>
    
    <tr><td>Enable/Disable hook</td>
    <td><center>
    
    <input type="checkbox" name="hook_left" <?php if($_POST['hook_left'] == "on" OR Configuration::get('hook_left') == "on") { echo " checked";} ?>/> &nbsp;
        
    </center></td></tr>
    
     
    <tr>
    <td width="40%">List title</td>
    <td width="60%">
    <select name="list_title_left">
    <option value="normal" <?php if($_POST['list_title_left'] == 'normal' OR Configuration::get('list_title_left') == "normal") {echo " selected='selected'";}?> >Feature Products</option>
    <option value="cheap" <?php if($_POST['list_title_left'] == 'cheap' OR Configuration::get('list_title_left') == "cheap") {echo " selected='selected'";}?> >Cheap Products</option>
    <option value="new" <?php if($_POST['list_title_left'] == 'new' OR Configuration::get('list_title_left') == "new") {echo " selected='selected'";}?> >New Products</option>
    <option value="popular" <?php if($_POST['list_title_left'] == 'popular' OR Configuration::get('list_title_left') == "popular") {echo " selected='selected'";}?> >Most Popular</option>
    <option value="best" <?php if($_POST['list_title_left'] == 'best' OR Configuration::get('list_title_left') == "best") {echo " selected='selected'";}?> >Best Seller</option>
    <option value="random" <?php if($_POST['list_title_left'] == 'random' OR Configuration::get('list_title_left') == "random") {echo " selected='selected'";}?> >Random</option>
    </select>
    </td>
    </tr>
    
    <tr><td>Product's price</td>
    <td><center>
    
    <input type="checkbox" name="product_price_left" <?php if($_POST['product_price_left'] == "on" OR Configuration::get('option_price_left') == "on") {echo " checked";} ?>/> &nbsp;
    
    
    </center></td></tr>
    
    <tr>
    <td>Add to cart button</td>
    <td align="center">
    <input type="checkbox" name="addtocart_left" <?php if($_POST['addtocart_left'] == "on" OR Configuration::get('option_cart_left') == "on") { echo " checked";} ?>/> &nbsp;
    </td>
    </tr>
    </table>
    </td>
   
    <td align="center">
    <table class="table" width="95%">
    <tr><td>Enable/Disable hook</td>
    <td align="center">   
    <input type="checkbox" name="hook_right" <?php if($_POST['hook_right'] == "on" OR Configuration::get('hook_right') == "on") { echo " checked";} ?>/> &nbsp;
    </td></tr>
    <tr>
    <td width="40%">List title</td>
    <td width="60%">
    <select name="list_title_right">
    <option value="normal" <?php if($_POST['list_title_right'] == 'normal' OR Configuration::get('list_title_right') == "normal") {echo " selected='selected'";}?> >Feature Products</option>
    <option value="cheap" <?php if($_POST['list_title_right'] == 'cheap' OR Configuration::get('list_title_right') == "cheap") {echo " selected='selected'";}?> >Cheap Products</option>
    <option value="new" <?php if($_POST['list_title_right'] == 'new' OR Configuration::get('list_title_right') == "new") {echo " selected='selected'";}?> >New Products</option>
    <option value="popular" <?php if($_POST['list_title_right'] == 'popular' OR Configuration::get('list_title_right') == "popular") {echo " selected='selected'";}?> >Most Popular</option>
    <option value="best" <?php if($_POST['list_title_right'] == 'best' OR Configuration::get('list_title_right') == "best") {echo " selected='selected'";}?> >Best Seller</option>
    <option value="random" <?php if($_POST['list_title_right'] == 'random' OR Configuration::get('list_title_right') == "random") {echo " selected='selected'";}?> >Random</option>
    </select>
    </td>
    </tr>
    
 
    <tr><td>Product's price</td>
    <td align="center">    
    <input type="checkbox" name="product_price_right" <?php if($_POST['product_price_right'] == "on" OR Configuration::get('option_price_right') == "on") {echo " checked";} ?>/> &nbsp;
    </td></tr>
    <tr>
    <td>Add to cart button</td>
    <td align="center">    
    <input type="checkbox" name="addtocart_right" <?php if($_POST['addtocart_right'] == "on" OR Configuration::get('option_cart_right') == "on") { echo " checked";} ?>/> &nbsp;
    </td>
    </tr>
    </table>
    
    </td>
    </tr>
    </table>
    
    <hr />
    <table width="100%" class="table_noborder">
    <tr>
    <td align="right"><input type="submit" name="save_option" value="Save" /></td>
    </tr>
    </table> 
    
    
    </form>
    

