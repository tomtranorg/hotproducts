<?php
class PromoteProducts extends Module
    {
/*
 * This is construct method that set value of some variables.
 */
        public function __construct()
        {
            $this->name = 'promoteProducts';
            $this->tab = 'front_office_features';
            $this->version = '1.0';
            $this->author = 'GeekPolis';
            
            parent::__construct();
            
            $this->displayName = $this->l('Promote Products');
            $this->description = $this->l('You can add products to display in block on front page.');
            $this->confirmUninstall = $this->l('Are you sure uninstall this module?');
        }
/*
 * This method will called when this module's installed.
 */ 
        public function install()
        {
            $query = 'ALTER TABLE '._DB_PREFIX_.'image ADD url varchar(255) NOT NULL default ""';
            $query2 = 'ALTER TABLE '._DB_PREFIX_.'product_lang ADD status int(1) NOT NULL default 1';
           if(!parent::install() OR !$this->registerHook('header') OR !$this->registerHook('home') OR !$this->registerHook('leftColumn') OR !$this->registerHook('rightColumn') OR !Db::getInstance()->Execute($query) OR !Db::getInstance()->Execute($query2)) //OR !Db::getInstance()->Execute($query2))
           {
            return false;
           } 
           else
           {
            Configuration::updateValue('hook_header', 'on');
            Configuration::updateValue('list_title_header', '');
            Configuration::updateValue('option_price_header', 'on');
            Configuration::updateValue('option_cart_header', 'on');
            
            Configuration::updateValue('hook_home', 'on');
            Configuration::updateValue('list_title_home', '');
            Configuration::updateValue('option_price_home', 'on');
            Configuration::updateValue('option_cart_home', 'on');
            
            Configuration::updateValue('hook_left', 'on');
            Configuration::updateValue('list_title_left', '');
            Configuration::updateValue('option_price_left', 'on');
            Configuration::updateValue('option_cart_left', 'on');
            
            Configuration::updateValue('hook_right', 'on');
            Configuration::updateValue('list_title_right', '');
            Configuration::updateValue('option_price_right', 'on');
            Configuration::updateValue('option_cart_right', 'on');
            
            Configuration::updateValue('number_cheap', '5');
            Configuration::updateValue('number_new', '5');
            Configuration::updateValue('number_popular', '5');
            Configuration::updateValue('number_bestsell', '5');
            Configuration::updateValue('number_random', '5');
            
            Configuration::updateValue('feature', 'normal');
                                    
            return true;
           }
           
        }
/*
 *  This method will called when this module's uninstalled.
 */        
        public function uninstall()
        {
            $query = 'ALTER TABLE '._DB_PREFIX_.'image DROP url';
            $query2 = 'ALTER TABLE '._DB_PREFIX_.'product_lang DROP status';
            if(!parent::uninstall() OR !Db::getInstance()->Execute($query) OR !Db::getInstance()->Execute($query2))
            {
                return false;
            }
            else
            {
                return true;
            }
        
        }
        
 /*
  * Display module's admin form.
  */
        public function getContent()
            {
           
                
                $url = $_SERVER['REQUEST_URI'];
                $feature = Configuration::get('feature');
                
                echo '<form action="" method="POST">
                <table class="table_noborder">
                <tr>';
                echo '<td><a href="';
                echo $url.'&p=general&trg=1" title="General">General&nbsp;</a></td>';
                echo '<td><a href="';
                echo $url.'&p=option" title="Option">Option</a></td>';
                echo '</tr>
                </table>
                </form><hr>';
                
                if (isset($_GET['p']))
                     {
                        if ($_GET['p'] == 'general')
                        {
                         include "general.php";
                        }
                        elseif ($_GET['p'] == 'option')
                        {
                         include "option.php";   
                        }
                     }
                else
                    {
                       Tools::redirectAdmin($url.'&p=general&trg=1');
                    }
                

                  
                  if (Tools::isSubmit("save_option"))
                      {
                    
                                $this->setOption(Tools::getValue('hook_header'), Tools::getValue('list_title_header'), Tools::getValue('product_price_header'), Tools::getValue('addtocart_header'),
                                                 Tools::getValue('hook_home'), Tools::getValue('list_title_home'), Tools::getValue('product_price_home'), Tools::getValue('addtocart_home'),
                                                 Tools::getValue('hook_left'), Tools::getValue('list_title_left'), Tools::getValue('product_price_left'), Tools::getValue('addtocart_left'),
                                                 Tools::getValue('hook_right'), Tools::getValue('list_title_right'), Tools::getValue('product_price_right'), Tools::getValue('addtocart_right'),
                                                 Configuration::get('number_cheap'),Configuration::get('number_new'), Configuration::get('number_popular'), Configuration::get('number_bestsell'), Configuration::get('number_random'),
                                                 Configuration::get('feature'));
                                
                      }
                 
               
            }
            
        
 /*
  * Hook this module to Header position
  * Contain javascript and css files link.
  */  
        public function hookHeader($params)
            {
                global $smarty;
                
                $sql_img = "SELECT id_image FROM "._DB_PREFIX_."image WHERE position=1";
    			$img_query = mysql_query($sql_img);
    			$images = array();
                                
    			while($id_image = mysql_fetch_array($img_query))
                    {
        				$image = new Image($id_image['id_image']);
                		$image_url = _THEME_PROD_DIR_.$image->getExistingImgPath();
                        mysql_query("Update "._DB_PREFIX_."image set url='$image_url' where id_image='$id_image[id_image]'");
        			       				
        			}
                                
                $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                FROM ps_image, ps_product_lang, ps_product
                WHERE ps_image.id_product = ps_product_lang.id_product 
                AND ps_product.id_product=ps_product_lang.id_product 
                AND ps_image.position = 1 
                AND ps_product_lang.id_lang = 1 
                ";
                                                
                if(Configuration::get('list_title_header') == 'popular')
                    {
                        $nums = Configuration::get('number_popular');
                        $sql_img2 .= " ORDER BY ps_product.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_header') == 'cheap')
                    {
                        $nums = Configuration::get('number_cheap');
                        $sql_img2 .= " ORDER BY ps_product.price ASC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_header') == 'new')
                    {
                        $nums = Configuration::get('number_new');
                        $sql_img2 .= " ORDER BY ps_product.date_add DESC limit 0,$nums";
                    }
                                                
                elseif(Configuration::get('list_title_header') == 'bestsell')
                    {
                        $nums = Configuration::get('number_bestsell');
                        $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                                    FROM ps_image, ps_product_lang, ps_product, ps_product_sale
                                    WHERE ps_image.id_product = ps_product_lang.id_product 
                                    AND ps_product.id_product=ps_product_lang.id_product 
                                    AND ps_product_sale.id_product = ps_product.id_product
                                    AND ps_image.position = 1 
                                    AND ps_product_lang.id_lang = 1 
                                    AND ps_product_lang.status = 1 
                                    ORDER BY ps_product_sale.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_header') == 'random')
                    {
                        $nums = Configuration::get('number_random');
                        $sql_img2 .= " ORDER BY RAND() limit 0,$nums";
                    }
                else
                    {
                        $sql_img2 .= " AND ps_product_lang.status = 1 
                                       ORDER BY ps_product.id_product ASC";
                    }
                    
                $product_info = array();    
    			$img_query2 = mysql_query($sql_img2);
                while($id_image2 = mysql_fetch_array($img_query2))
                    {
                        $product_info[] = $id_image2;
                    }
                
                $smarty->assign('product_info', $product_info);
                $smarty->assign('sql', $sql_img2);
                $smarty->assign('number', Configuration::get('number_random'));
                $smarty->assign('hook_header', Configuration::get('hook_header'));             
                $smarty->assign('list_title_header', Configuration::get('list_title_header'));
                $smarty->assign('feature',Configuration::get('feature'));
			                
                Tools::addJS(($this->_path).'js/jquery.jcarousel.min.js');
                Tools::addCSS(($this->_path).'css/skin.css', 'all');
                Tools::addCSS(($this->_path).'css/style.css', 'all');
                
                return $this->display(__FILE__, 'header.tpl');
                     
            }
            
/*
 *  Hook this module to Home position.
 */
         public function hookHome($params)
            {
                global $smarty;
                              
                $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                FROM ps_image, ps_product_lang, ps_product
                WHERE ps_image.id_product = ps_product_lang.id_product 
                AND ps_product.id_product=ps_product_lang.id_product 
                AND ps_image.position = 1 
                AND ps_product_lang.id_lang = 1 
                ";
                if(Configuration::get('list_title_home') == 'popular')
                    {
                        $nums = Configuration::get('number_popular');
                        $sql_img2 .= " ORDER BY ps_product.quantity DESC limit 0,$nums";
                    }
               
                elseif(Configuration::get('list_title_home') == 'cheap')
                    {
                        $nums = Configuration::get('number_cheap');
                        $sql_img2 .= " ORDER BY ps_product.price ASC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_home') == 'new')
                    {
                        $nums = Configuration::get('number_new');
                        $sql_img2 .= " ORDER BY ps_product.date_add DESC limit 0,$nums";
                    }
                              
                elseif(Configuration::get('list_title_home') == 'bestsell')
                    {
                        $nums = Configuration::get('number_bestsell');
                        $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                                    FROM ps_image, ps_product_lang, ps_product, ps_product_sale
                                    WHERE ps_image.id_product = ps_product_lang.id_product 
                                    AND ps_product.id_product=ps_product_lang.id_product 
                                    AND ps_product_sale.id_product = ps_product.id_product
                                    AND ps_image.position = 1 
                                    AND ps_product_lang.id_lang = 1 
                                    AND ps_product_lang.status = 1 
                                    ORDER BY ps_product_sale.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_home') == 'random')
                    {
                        $nums = Configuration::get('number_random');
                        $sql_img2 .= " ORDER BY RAND() limit 0,$nums";
                    }
                else
                    {
                        $sql_img2 .= " AND ps_product_lang.status = 1 
                                       ORDER BY ps_product.id_product ASC";
                    }
                    
                $product_info = array();    
    			$img_query2 = mysql_query($sql_img2);
                while($id_image2 = mysql_fetch_array($img_query2))
                    {
                        $product_info[] = $id_image2;
                    }
                
                $smarty->assign('product_info', $product_info);
                $smarty->assign('sql', $sql_img2);
                $smarty->assign('hook_home', Configuration::get('hook_home'));             
                $smarty->assign('list_title_home', Configuration::get('list_title_home'));
                return $this->display(__FILE__, 'home.tpl');
              
            }
/*
 * Hook this module to LeftColumn
 */       
        public function hookLeftColumn($params)
            {
                global $smarty;
                                               
                $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                FROM ps_image, ps_product_lang, ps_product
                WHERE ps_image.id_product = ps_product_lang.id_product 
                AND ps_product.id_product=ps_product_lang.id_product 
                AND ps_image.position = 1 
                AND ps_product_lang.id_lang = 1 
                ";
                
                if(Configuration::get('list_title_left') == 'popular')
                    {
                        $nums = Configuration::get('number_popular');
                        $sql_img2 .= " ORDER BY ps_product.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_left') == 'cheap')
                    {
                        $nums = Configuration::get('number_cheap');
                        $sql_img2 .= " ORDER BY ps_product.price ASC limit 0,$nums";
                    }
               
                elseif(Configuration::get('list_title_left') == 'new')
                    {
                        $nums = Configuration::get('number_new');
                        $sql_img2 .= " ORDER BY ps_product.date_add DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_left') == 'bestsell')
                    {
                        $nums = Configuration::get('number_bestsell');
                        $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                                    FROM ps_image, ps_product_lang, ps_product, ps_product_sale
                                    WHERE ps_image.id_product = ps_product_lang.id_product 
                                    AND ps_product.id_product=ps_product_lang.id_product 
                                    AND ps_product_sale.id_product = ps_product.id_product
                                    AND ps_image.position = 1 
                                    AND ps_product_lang.id_lang = 1 
                                    AND ps_product_lang.status = 1 
                                    ORDER BY ps_product_sale.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_left') == 'random')
                    {
                        $nums = Configuration::get('number_random');
                        $sql_img2 .= " ORDER BY RAND() limit 0,$nums";
                    }
                else
                    {
                        $sql_img2 .= " AND ps_product_lang.status = 1 
                                       ORDER BY ps_product.id_product ASC";
                    }
                    
                $product_info = array();    
    			$img_query2 = mysql_query($sql_img2);
                while($id_image2 = mysql_fetch_array($img_query2))
                    {
                        $product_info[] = $id_image2;
                    }
                
                $smarty->assign('product_info', $product_info);
                $smarty->assign('sql', $sql_img2);
                $smarty->assign('hook_left', Configuration::get('hook_left'));             
                $smarty->assign('list_title_left', Configuration::get('list_title_left'));
                
                return $this->display(__FILE__, 'left_column.tpl');
                
            }
 /*
  * Hook this module to Rightcolumn.
  */
        public function hookRightColumn($params)
            {
                global $smarty;
                
                $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                FROM ps_image, ps_product_lang, ps_product
                WHERE ps_image.id_product = ps_product_lang.id_product 
                AND ps_product.id_product=ps_product_lang.id_product 
                AND ps_image.position = 1 
                AND ps_product_lang.id_lang = 1 
                ";
                
                if(Configuration::get('list_title_right') == 'popular')
                    {
                        $nums = Configuration::get('number_popular');
                        $sql_img2 .= " ORDER BY ps_product.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_right') == 'cheap')
                    {
                        $nums = Configuration::get('number_cheap');
                        $sql_img2 .= " ORDER BY ps_product.price ASC limit 0,$nums";
                    }
              
                elseif(Configuration::get('list_title_right') == 'new')
                    {
                        $nums = Configuration::get('number_new');
                        $sql_img2 .= " ORDER BY ps_product.date_add DESC limit 0,$nums";
                    }
               elseif(Configuration::get('list_title_right') == 'bestsell')
                    {
                        $nums = Configuration::get('number_bestsell');
                        $sql_img2 = "SELECT ps_image.url as url, ps_product.price as price, ps_product_lang.name as name, ps_image.id_product as id_product 
                                    FROM ps_image, ps_product_lang, ps_product, ps_product_sale
                                    WHERE ps_image.id_product = ps_product_lang.id_product 
                                    AND ps_product.id_product=ps_product_lang.id_product 
                                    AND ps_product_sale.id_product = ps_product.id_product
                                    AND ps_image.position = 1 
                                    AND ps_product_lang.id_lang = 1 
                                    AND ps_product_lang.status = 1 
                                    ORDER BY ps_product_sale.quantity DESC limit 0,$nums";
                    }
                
                elseif(Configuration::get('list_title_right') == 'random')
                    {
                        $nums = Configuration::get('number_random');
                        $sql_img2 .= " ORDER BY RAND() limit 0,$nums";
                    }
                else
                    {
                        $sql_img2 .= " AND ps_product_lang.status = 1 
                                       ORDER BY ps_product.id_product ASC";
                    }
                    
                $product_info = array();    
    			$img_query2 = mysql_query($sql_img2);
                while($id_image2 = mysql_fetch_array($img_query2))
                    {
                        $product_info[] = $id_image2;
                    }
                
                $smarty->assign('product_info', $product_info);
                $smarty->assign('sql', $sql_img2);
                $smarty->assign('hook_right', Configuration::get('hook_right'));             
                $smarty->assign('list_title_right', Configuration::get('list_title_right'));
                
                return $this->display(__FILE__, 'right_column.tpl');
                
            }
 /*
  * This function set variables configuration for hooks position.
  */
        public function setOption($hook_header=NULL, $list_title_header=NULL, $option_price_header=NULL, $option_cart_header=NULL,
                                  $hook_home=NULL, $list_title_home=NULL, $option_price_home=NULL, $option_cart_home=NULL,
                                  $hook_left=NULL, $list_title_left=NULL, $option_price_left=NULL, $option_cart_left=NULL,
                                  $hook_right=NULL, $list_title_right=NULL, $option_price_right=NULL, $option_cart_right=NULL,
                                  $number_cheap=NULL, $number_new=NULL, $number_popular=NULL, $number_bestsell=NULL, $number_random=NULL,
                                  $feature=NULL)   
            {
                Configuration::updateValue('hook_header', $hook_header);
                Configuration::updateValue('list_title_header', $list_title_header);
                Configuration::updateValue('option_price_header', $option_price_header);
                Configuration::updateValue('option_cart_header', $option_cart_header);
                
                Configuration::updateValue('hook_home', $hook_home);
                Configuration::updateValue('list_title_home', $list_title_home);
                Configuration::updateValue('option_price_home', $option_price_home);
                Configuration::updateValue('option_cart_home', $option_cart_home);
                
                Configuration::updateValue('hook_left', $hook_left);
                Configuration::updateValue('list_title_left', $list_title_left);
                Configuration::updateValue('option_price_left', $option_price_left);
                Configuration::updateValue('option_cart_left', $option_cart_left);
                
                Configuration::updateValue('hook_right', $hook_right);
                Configuration::updateValue('list_title_right', $list_title_right);
                Configuration::updateValue('option_price_right', $option_price_right);
                Configuration::updateValue('option_cart_right', $option_cart_right);
                
                Configuration::updateValue('number_cheap', $number_cheap);
                Configuration::updateValue('number_new', $number_new);
                Configuration::updateValue('number_popular', $number_popular);
                Configuration::updateValue('number_bestsell', $number_bestsell);
                Configuration::updateValue('number_random', $number_random);
                
                Configuration::updateValue('feature', $feature);
                
                
            }
            
               
    }

?>
